#!/usr/bin/python3

import os
import subprocess
import argparse

import chosheet

TARGET_DIR = 'pdf'

ap = argparse.ArgumentParser()
ap.add_argument('name', nargs='?')
ap.add_argument('-f', '--force', action='store_true')
ap.add_argument('-c', '--config', default='chordpro.conf.json')
ap.add_argument('-v', '--view', action='store_true')
args = ap.parse_args()

pdf_files = list()


def process_file(fn, force=False, pageno=None):
  cs = chosheet.ChoSheet(fn, pdf_fn=os.path.join(TARGET_DIR, os.path.basename(fn)) + '.pdf')
  if force or (not cs.is_pdf_current()):
    date = cs.cho_timestamp().strftime('%d.%m.%Y')
    if pageno:
      if pageno % 2 == 0:
        footer = [pageno, '', date]
      else:
        footer = [date, '', pageno]
    else:
      footer = [date, '', '']
    if not os.path.isdir(TARGET_DIR):
      os.mkdir(TARGET_DIR)
    cs.render_pdf(footer=footer, chordpro_conf=args.config)
  pdf_files.append(cs.pdf_fn)
   

if args.name:
  n = args.name
  if n.endswith('.cho'):
    n = n[0:-4]
  process_file(n, force=True)
else:
  c = 0
  for fn in chosheet.files():
    c += 1
    process_file(fn, force=args.force, pageno=c+2)
  print('processed %d files' % c)

if args.view:
  newest = [0, None]
  for fn in pdf_files:
    mtime = os.lstat(fn).st_mtime
    if mtime > newest[0]:
      newest[0] = mtime
      newest[1] = fn
  subprocess.call(['evince', newest[1]])
