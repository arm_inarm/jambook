FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive

# some preseed
RUN echo "tzdata tzdata/Areas select Europe" | debconf-set-selections; \
  echo "tzdata tzdata/Zones/Europe select Berlin" | debconf-set-selections; \
  echo "locales locales/locales_to_be_generated multiselect de_DE.UTF-8 UTF-8" | debconf-set-selections; \
  echo "locales locales/default_environment_locale select de_DE.UTF-8" | debconf-set-selections

# GENERAL
RUN apt-get -y -q update && \
  apt-get install -y --no-install-recommends apt-utils iputils-ping dnsutils iproute2 net-tools telnet wget curl mysql-client tcpdump openssh-server vim git man tzdata locales openssl

RUN mkdir -p /root/.ssh && \
  echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPiRwfuCc98FbFXhadNJwAav81EVdgp8KEH88X5GLhYZdKqf0AFtaRoEod5qIilSrbol+w2tC/CtX0bSmN+dMyDx7ELZoKWe2BEyrkJc1w4fUunaabB0NBWIzoZLZCLchQ18gTRX8IDb0Uj2HhFp2DCAr6sFUVBERAKjbCOZTaUqlICmwFFJwwa8FFzoEbr0FbHgeFUqSA+WaK6ZNBCFZp9SJLkP/dXjJ7Yq3ToZD1FiHgbDCZ6lbc04Ztqo8h0Meizmbmv8ZMs3J1t1zdTFJa5qqS7oxO+GvL0Bpf4q26eZrsecEfz2Uf5E+Tx9TQwi5UBYQGLIrkTfSAHCGqxahZ Tobias Galitzien" > /root/.ssh/authorized_keys

RUN touch /root/.hushlogin
RUN mkdir /var/run/sshd
 
RUN echo "#\!/bin/bash\ngit clone https://gitlab.com/oso-berlin/jambook.git" > /root/clone; chmod +x /root/clone

ENTRYPOINT printenv > /run/k8s_env; /usr/sbin/sshd -D
